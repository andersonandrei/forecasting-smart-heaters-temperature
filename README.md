# Short-Term Ambient Temperature Forecasting for Smart Heaters

This repository is focused on the research project **Short-Term Ambient Temperature Forecasting for Smart Heaters**.

The **authors** are:
Danilo Carastan-Santos, Anderson Andrei Da Silva, Alfredo Goldman, Angan Mitra, Clément Mommessin, Yanik Ngoko, Denis Trystram

Please, find here:
- documentation, aka, the manunscript and the LaTex project on *'doc/'*,
- experimental environment on '*experiments/*',
- results and plots on *'experiments/results/*'.

## Experiments

### Before running

It is required the following packages (linux):
- libcurl4-openssl-dev 
- libxml2-dev
- curl
- libv8-dev

Install them by: ```sudo apt-get install libcurl4-openssl-dev libxml2-dev curl libv8-dev ```

All the experiments have been written in R code. So, to run them on a linux terminal, replace the *script.r* by the desired script and execute as follow: ```Rscript script.r```

The following libraries (R) are required:
- dplyr
- tidyr
- ggplot2
- forecast
- prophet
- data.table
- patchwork
- Metrics
- RColorBrewer

Then, install Install them by:
- Accessing a terminal (R),
- Command: ```install.package('library')```

### Methodology Description

Our methodology will be described as the steps illustrated in: 

![Forecasting Methodology](./forecasting_methodology_diagram.png)

#### Running the experiments:

1) **Data Preprocessing**:
	- The file is in: preprocessing/data_preprocessing.r (please, notice that the csv should use ',' as a separator.)
	- It receives as input the input_data/input_data.csv
	- It outputs input_data/filtered_input_data.csv


2) **Hyperparameter Tuning**:

	2.1) Run the **hyperparameter_tuning** phase:
	- The files are in:
		- hyperparameter_tuning/hyperparameter_tuning_model_1.r
		- hyperparameter_tuning/hyperparameter_tuning_model_2.r
	- They receive as input input_data/filtered_input_data.csv
	- They output:
		- results/hyperparameter_tuning/model_1/additive/results_tuning_additive_model_1.csv
		- results/hyperparameter_tuning/model_1/multiplicative/results_tuning_multiplicative_model_1.csv
		- results/hyperparameter_tuning/model_2/additive/results_tuning_additive_model_2.csv
		- results/hyperparameter_tuning/model_2/multiplicative/results_tuning_multiplicative_model_2.csv


	2.2) Run the **hyperparameter_evaluation** phase:
	- The file is in hyperparameter_evaluation/hyperparameters_evaluation.r
	- It receives as input:
		- results/hyperparameter_tuning/model_1/additive/results_tuning_additive_model_1.csv
		- results/hyperparameter_tuning/model_1/multiplicative/results_tuning_multiplicative_model_1.csv
		- results/hyperparameter_tuning/model_2/additive/results_tuning_additive_model_2.csv
		- results/hyperparameter_tuning/model_2/multiplicative/results_tuning_multiplicative_model_2.csv
	- It outputs:
		- csv files:
			- results/hyperparameter_evaluation/[]_scale_effect_.csv (with the effect of each isolated hyperparameters by RMSE and MAPE)
			- results/hyperparameter_evaluation/hyperparameters_results_[].csv (with the ranking of the hyperparameters set by RMSE and MAPE)
		- png and pdf files:
			- results/hyperparameter_evaluation/hyperparameters_evaluation_[].png (with the plots of the hyperparameters analysis by RMSE and MAPE)


3) **Sliding Window Temperature Forecasting**:

	3.1) Look at results/hyperparameter_evaluation/hyperparameters_results_model_[]_mape.csv and results/hyperparameter_evaluation/hyperparameters_results_model_[]_rmse.csv:
	- Compare such results and select the best set of hyperparameters, for each model, to execute the next phase.


	3.2) Run the **forecating** phase:
	- The files are in:
		- forecasting/forecasting_model_1.r
		- forecasting/forecasting_model_2.r
	- They receive as input:
		- input_data/filtered_input_data.csv
	- They need to be modified based on the best set of hyperparameters selected in the previous phase:
		- changepoint_prior_scale
		- seasonality_prior_scale
		- fourier_order
		- mode
	- They output:
		- results/forecasting/df_predictions_model_1.csv
		- results/forecasting/df_predictions_model_2.csv


4) Run the **forecasting_analysis** phase (extra illustration):
	- The file is in forecasting_analysis/forecasting_results_analysis.r
	- It receive as input: 
		- input_data/filtered_input_data.csv
		- results/df_predictions_model_1.csv
		- results/df_predictions_model_2.csv
	- It outputs:
		All the plots regarding the forecasts:
		- forecasts_by_intervals_with_point
		- forecasts_by_intervals_with_lines
		- forecasts_all_components_first_interval
		- forecasts_all_components
		- forecasts_all_period
		- error_analysis_by_model
		- error_analysis_model_2
		- illustrate_methodology_plots

## Contact

For any question, please, don't hesitate to contact me:

- Anderson Andrei: silva.andersonandrei@gmail.com, anderson-andrei.da-silva@inria.fr