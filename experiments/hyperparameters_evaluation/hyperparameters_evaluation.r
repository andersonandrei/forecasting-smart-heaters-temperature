# Importing libraries

library(dplyr)
library(tidyr)
library(ggplot2)
library(data.table)

# Functions definition

## Read and bind all files
read_and_bind_all_files <- function(path, pattern = "*.csv") {
    files = list.files(path, pattern, full.names = TRUE)
    rbindlist(lapply(files, function(x) 
                     (fread(x) )))
}
                     
## To list the name of the files. I will get the name of the qrads which comes from qrad_code.csv
list_qrads <- function(path, pattern = "*") {
    file_name <- list.files(path, pattern, full.names = TRUE)
}           

## Mode hyperparameter analysis
mode_analysis <- function() {
    print('mode_analysis')
    metrics <- c('rmse','mape')

    for (metric in metrics) {
        print(metric)

        # Bindig the additive and multiplicative results
        df_results_all <- rbind(df_all_results_additive, df_all_results_multiplicative)
        index <- seq(1, dim(df_results_all)[1])
        df_results_all <- cbind(index, df_results_all)

        # To select the desired variables
        df_results_all_filtered <- df_results_all %>% 
                    select(-start,-end,-horizon,-mse,-mae,-coverage)

        # To gather the metrics in only one column
        df_results_all_gathered <- df_results_all_filtered %>% 
                        gather(
                            factor_key = TRUE, 
                            key = "metric_name", 
                            value = "metric_values",
                            -index, -changepoint_prior_scale, -seasonality_prior_scale, -fourier_order, -mode)

        # To plot all the results together {additive, multiplicative}
        df_plot <- df_results_all_gathered %>% filter(metric_name == metric)
        ggplot(df_plot, aes(x = index, y = metric_values, color = as.factor(mode))) +
            geom_point() +
            labs(x = "Experiment Index", y = metric, color = "Mode") +
            scale_color_brewer(palette="Set2") +
            theme_bw() +
            ggsave(paste('../results/hyperparameter_evaluation/additive_multiplicative_results_', model, '_', metric, '.png', sep=''))

        # To plot only the additive results
        df_plot <- df_plot %>% filter(mode == 1)
        ggplot(df_plot, aes(x = index, y = metric_values)) +
            geom_point() +
            labs(x = "Experiment Index", y = metric) +
            scale_color_brewer(palette="Set2") +
            theme_bw() +
            ggsave(paste('../results/hyperparameter_evaluation/additive_results_', model, '_', metric, '.png', sep=''))
    }    
}

## Hyperparameter analysis (not considering the mode)
hyperparameter_evaluation <- function() {
    print('hyperparameter_evaluation')
    metrics <- c('rmse','mape')

    for (metric in metrics) {
        #print(metrics[metric_row,][2])
        print(metric)

        # To select the desired variables
        df_results_all_filtered <- df_all_results_additive %>% 
                    select(-start,-end,-horizon,-mse,-mae,-coverage, -mode)

        # To gather the metrics in only one column
        df_results_gathered_metrics <- df_results_all_filtered %>% 
                                        gather(
                                            factor_key = TRUE, 
                                            key = "metric_name", 
                                            value = "metric_values",
                                            -changepoint_prior_scale, -seasonality_prior_scale, -fourier_order) %>% 
                                        filter(metric_name == metric)

        # To do the isolated hyperparameter analysis            
        
        ## changepoint prior scale effect
        df_grouped_changepoint <- df_results_gathered_metrics %>% 
                    group_by(changepoint_prior_scale) %>%
                    summarize(mean = mean(metric_values), sd = sd(metric_values))
        write.csv(df_grouped_changepoint, paste('../results/hyperparameter_evaluation/changepoint_prior_scale_effect_', model, '_', metric, '.csv', sep=''), row.names = TRUE)

        ## seasonality prior scale effect
        df_grouped_seasonality <- df_results_gathered_metrics %>% 
                        group_by(seasonality_prior_scale) %>%
                        summarize(mean_rmse = mean(metric_values), sd_rmse = sd(metric_values))
        write.csv(df_grouped_seasonality, paste('../results/hyperparameter_evaluation/seasonality_prior_scale_effect_', model, '_', metric, '.csv', sep=''), row.names = TRUE)

        ## fourier order effect
        df_grouped_fourier <- df_results_gathered_metrics %>% 
                        group_by(fourier_order) %>%
                        summarize(mean = mean(metric_values), sd = sd(metric_values))
        write.csv(df_grouped_fourier, paste('../results/hyperparameter_evaluation/fourier_order_effect_', model, '_', metric, '.csv', sep=''), row.names = TRUE)


        # To do the general hyperparameter analysis
        
        ## To group by all hyperparameters
        df_grouped <- df_results_gathered_metrics %>% 
                        select(changepoint_prior_scale, seasonality_prior_scale, fourier_order, metric_values) %>%
                        group_by(changepoint_prior_scale, seasonality_prior_scale, fourier_order) %>%
                        summarize(mean = mean(metric_values), sd = sd(metric_values))

        ## To label by the seasonality. It will be used on the facets of the plot
        df_grouped <- df_grouped %>% 
                        group_by(seasonality_prior_scale) %>%
                          mutate(seasonality_prior_scale_label = paste('Seasonality Prior Scale = ', as.factor(seasonality_prior_scale)))
        
        ## To convert the values to factor
        df_grouped$fourier_order <- as.factor(df_grouped$fourier_order)
        df_grouped$changepoint_prior_scale <- as.factor(df_grouped$changepoint_prior_scale)

        ## To order the data frame by mean and save as csv
        df_grouped_sorted <- arrange(df_grouped, mean)
        write.csv(df_grouped_sorted, paste('../results/hyperparameter_evaluation/hyperparameters_results_', model, '_', metric, '.csv', sep=''), row.names = TRUE)

        ## To plot
        ggplot(df_grouped, 
                aes(x = changepoint_prior_scale, y = mean, shape = fourier_order,)) +
                geom_point(aes(changepoint_prior_scale), position=position_dodge(width=0.5), size = 2) +
                geom_errorbar(aes(ymin = mean - sd, ymax = mean + sd), width = 0.1, position=position_dodge(width=0.5)) +
                facet_wrap(seasonality_prior_scale_label ~ ., nrow = 4) + 
                labs(x = "Changepoint Prior Scale", y = paste(" Mean", metric), shape = "Fourier Order") +
                theme_bw() +
                theme(
                      strip.text.x = element_text(size = 12),
                      legend.position= "bottom",
                      axis.text=element_text(size=14),
                      axis.title=element_text(size=14),
                      legend.title=element_text(size=14), 
                      legend.text=element_text(size=14))+
        ggsave(paste('../results/hyperparameter_evaluation/hyperparameters_evaluation_', model, '_', metric, '.png', sep='')) + 
        ggsave(paste('../results/hyperparameter_evaluation/hyperparameters_evaluation_', model, '_', metric, '.pdf', sep='')) 
    }
}

# Functions execution
models <- c('ambient_power_main', 'ambient_power_main_heatsink')
for (model_name in models){

    print(model_name)
    
    if (model_name == 'ambient_power_main') {
        model <- 'model_1'
    }
    else {
        model <- 'model_2'
    }
    df_all_results_additive <- read_and_bind_all_files(paste('../results/hyperparameter_tuning/', model, '/additive/', sep=''))
    df_all_results_multiplicative <- read_and_bind_all_files(paste('../results/hyperparameter_tuning/', model, '/multiplicative/', sep=''))
    df_all_results_multiplicative$mode <- 2
         
    mode_analysis()
    hyperparameter_evaluation()
}