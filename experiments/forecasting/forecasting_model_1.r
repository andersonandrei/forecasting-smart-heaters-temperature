# Importing libraries
library(dplyr)
library(tidyr)
library(ggplot2)
library(forecast)
library(prophet)
library(data.table)

df_rad <- read.csv(file = '../../input_data/filtered_input_data.csv', sep = ',', stringsAsFactors = FALSE)
df_rad$time <- as.POSIXct(df_rad$time,format='%Y-%m-%d %H:%M:%S', tz = "GMT")

df_temp <- df_rad %>% 
            select(time, ambient_temperature, power_instant_main) %>%
            rename(y = ambient_temperature, ds = time, power = power_instant_main) %>% 
            arrange(ds)


df_predictions <- data.frame()
df_predictions_full <- data.frame()
df_training_timestamps <- data.frame('ds' = df_temp$ds[1])
df_training_power <- data.frame()

start <- 0
middle <- start + 8640
end <- middle + 360
first_time <- 1
max_secs <- (as.integer(dim(df_temp)[1]))

while (end <= max_secs - 360) {
    print(start)
    if (first_time == 1) {
        print(start)
        print(middle)
        print(end)
        first_time <- 0
    }
    else {
        start <- start + 360
        middle <- middle + 360
        end <- end + 360
        print(start)
        print(middle)
        print(end)
    }

    df_hour <- df_temp[start:middle, ]
    
    m <- prophet(changepoint.prior.scale=0.01, seasonality.prior.scale=0.01, daily.seasonality=FALSE)
    m <- add_seasonality(m, name='daily_new', period=1, fourier.order=3)
    m <- add_regressor(m, 'power')
    m <- fit.prophet(m, df_hour)

    future <- make_future_dataframe(m, periods = 360, freq = 10)
    df_original_slice <- df_temp %>% select(ds, power)
    
    future <- left_join(future, df_original_slice, by = c('ds'='ds')) %>% arrange(desc(ds)) %>% distinct(ds, .keep_all = TRUE) %>% arrange(ds)
   
    power_aux <- 0
    for (i in 1 : dim(future)[1])  {
        line <- future[i,]
        if (is.na(line$power) == TRUE) {
            future[i, 2] <- power_aux
        }
        else {
            power_aux <- line$power
        }
    }

    #future$power[is.na(future$power)] = 0
    forecast <- predict(m, future)
    
    # Add [ds, forecasted]
    forecast_to_add <- forecast[(middle-start):dim(forecast)[1], ]
    df_predictions <- rbind(df_predictions, forecast_to_add)
    df_predictions_full <- rbind(df_predictions_full, forecast)
    
    # Add [ds, power]
    df_training_power <- rbind(df_training_power, future)
    
    # Add [ds] for training and forecasted data
    df_training_timestamps <- rbind(df_training_timestamps, 
                                    data.frame('ds' = df_temp$ds[middle])
                                   )
    df_training_timestamps <- rbind(df_training_timestamps, 
                                    data.frame('ds' = df_temp$ds[end])
                                   )
}

write.csv(df_predictions, '../results/forecasting/df_predictions_model_1.csv', row.names = FALSE)