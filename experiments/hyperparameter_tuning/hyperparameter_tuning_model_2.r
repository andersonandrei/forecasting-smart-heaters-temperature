# Importing libraries
library(dplyr)
library(tidyr)
library(ggplot2)
library(forecast)
library(prophet)

df_rad <- read.csv(file = '../input_data/filtered_input_data.csv', sep = ',', stringsAsFactors = FALSE)
summary(df_rad)
df_rad$time <- as.POSIXct(df_rad$time,format='%Y-%m-%d %H:%M:%S', tz = "GMT")
df_rad <- df_rad %>% arrange(time)
df_temp <- df_rad %>% select(time, ambient_temperature, power_instant_main, heatsink_temperature) %>%
                    rename(y = ambient_temperature, ds = time, power = power_instant_main, heatsink = heatsink_temperature)

summary(df_temp)
str(df_temp)

rand_search_grid = expand.grid(
    changepoint_prior_scale = c(0.01, 0.1, 1),
    seasonality_prior_scale = c(0.01, 0.1, 1, 10),
    fourier_order = c(10),
    fourier_order = c(1, 3, 5, 10),
    mode = c('additive', 'multiplicative')
)

#rand_search_grid
dim(rand_search_grid)

df_metrics <- data.frame()
for (i in 1:dim(rand_search_grid)[1]) {

    start <- 0
    middle <- 8640
    end <- 17280
    first_time <- 1

    while (end <= (as.integer(dim(df_temp)[1]) - 17280)) {
        if (first_time == 1) {
            print(start)
            print(middle)
            print(end)
            first_time <- 0
        }
        else {
            start <- start + 8640 + 8640
            middle <- middle + 8640 + 8640
            end <- end + 8640 + 8640
            print(start)
            print(middle)
            print(end)
        }

        experiment <- rand_search_grid[i,]
        print(experiment)

        df_day <- df_temp[start:end, ]

        m <- prophet(changepoint.prior.scale = experiment$changepoint_prior_scale,
                     seasonality.prior.scale = experiment$seasonality_prior_scale,
                     daily.seasonality=FALSE,
                     mode = experiment$mode)
        m <- add_seasonality(m, name='daily_new', period=1, fourier.order=experiment$fourier_order)
        m <- add_regressor(m, 'power')
        m <- add_regressor(m, 'heatsink')
        m <- fit.prophet(m, df_day)

        df.cv <- cross_validation(m, initial = 86400, horizon = 3600, period = 3600, units = 'secs')
        df.p <- performance_metrics(df.cv)
        write.csv(df.p,
                  paste('../results/hyperparameter_tuning/checkpoint_results_tuning_florestine_',
                    experiment$changepoint_prior_scale, '_',
                    experiment$seasonality_prior_scale, '_',
                    experiment$mode, '_',
                    experiment$fourier_order, '_',
                    start, '_', end, '.csv', sep=""),
                  row.names = FALSE)

        row <- cbind(experiment$changepoint_prior_scale,
                     experiment$seasonality_prior_scale,
                     experiment$fourier_order,
                     experiment$mode,
                     start,
                     end)

        row_results <- cbind(row, df.p[(nrow(df.p)):nrow(df.p), ])
        df_metrics <- rbind(df_metrics, row_results)
    }

    colnames(df_metrics)[1] <- "changepoint_prior_scale"
    colnames(df_metrics)[2] <- "seasonality_prior_scale"
    colnames(df_metrics)[3] <- "fourier_order"
    colnames(df_metrics)[4] <- "mode"
    colnames(df_metrics)[5] <- "start"
    colnames(df_metrics)[6] <- "end"

    #save the results as csv
    #write.csv(df_metrics,paste("results_tuning_"additive_10_ambient_power_main_heatsink.csv,"," sep=""), row.names = FALSE)
    write.csv(df_metrics, paste("../results/hyperparameter_tuning/model_2/", experiment$mode, "/results_tuning_", experiment$mode, "_model_2.csv", sep=""), row.names = TRUE)
}