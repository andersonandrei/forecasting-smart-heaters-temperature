# Importing libraries

library(dplyr)
library(tidyr)
library(ggplot2)
library(forecast)
library(prophet)
library(data.table)
library(patchwork)
library(Metrics)
library(RColorBrewer)

# Functions definition
plot_forecasts_by_intervals_with_point <- function() {

    df_plot <- df_result_forecasting_filtered
    start <- as.POSIXct("2019-11-20 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    end <- as.POSIXct("2020-01-03 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    a <- ggplot() +
        geom_point(data = df_plot, aes(x = ds, y = temperature_values, color = temperature_type), size = 0.5) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air Temperature ( ", degree*C, " )")), color = "Type of Temperature") +
        #scale_color_brewer(palette="Dark2") +
        xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(
          axis.title.x=element_blank(), 
          legend.position="none",
          #legend.position = c(0.60, 0.95),
          #legend.direction = "horizontal",
          #legend.background = element_rect(fill = "white", color = "gray"),
          legend.title=element_text(size=10), 
          legend.text=element_text(size=9)
        )
    start <- as.POSIXct("2020-01-03 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    end <- as.POSIXct("2020-02-06 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    b <- ggplot() +
        geom_point(data = df_plot, aes(x = ds, y = temperature_values, color = temperature_type), size = 0.5) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air Temperature ( ", degree*C, " )")), color = "Type of Temperature") +
        #scale_color_brewer(palette="Dark2") +
        xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(
          axis.title.x=element_blank(),
          legend.position="none",
          legend.title=element_text(size=10), 
          legend.text=element_text(size=9)
        )
    start <- as.POSIXct("2020-02-06 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    end <- as.POSIXct("2020-02-25 12:39:09",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    c <- ggplot() +
        geom_point(data = df_plot, aes(x = ds, y = temperature_values, color = temperature_type), size = 0.5) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air Temperature ( ", degree*C, " )")), color = "Type of Temperature") +
        #scale_color_brewer(palette="Dark2") +
        xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(
          legend.position="bottom",
          legend.box = "horizontal",
          legend.title=element_text(size=10), 
          legend.text=element_text(size=9)
        )
    plot <- a / b / c
    plot + ggsave('../results/forecasting_analysis/forecast_temperatures_by_intervals_points.png')
    plot + ggsave('../results/forecasting_analysis/forecast_temperatures_by_intervals_points.pdf')
}

plot_forecasts_by_intervals_with_lines <- function() {
    df_plot <- df_result_forecasting_filtered
    start <- as.POSIXct("2019-11-20 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    end <- as.POSIXct("2020-01-03 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    a <- ggplot() +
        geom_line(data = df_plot, aes(x = ds, y = temperature_values, color = temperature_type), size = 0.5) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air Temperature ( ", degree*C, " )")), color = "Type of Temperature") +
        #scale_color_brewer(palette="Dark2") +
        xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(
          axis.text=element_text(size=10),
          axis.title.x=element_blank(), 
          legend.position="none",
          #legend.position = c(0.60, 0.95),
          #legend.direction = "horizontal",
          #legend.background = element_rect(fill = "white", color = "gray"),
          legend.title=element_text(size=10), 
          legend.text=element_text(size=9)
        )
    start <- as.POSIXct("2020-01-03 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    end <- as.POSIXct("2020-02-06 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    b <- ggplot() +
        geom_line(data = df_plot, aes(x = ds, y = temperature_values, color = temperature_type), size = 0.5) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air Temperature ( ", degree*C, " )")), color = "Type of Temperature") +
        #scale_color_brewer(palette="Dark2") +
        xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(
          axis.text=element_text(size=10),
          axis.title.x=element_blank(),
          legend.position="none",
          legend.title=element_text(size=10), 
          legend.text=element_text(size=9)
        )
    start <- as.POSIXct("2020-02-06 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    end <- as.POSIXct("2020-02-25 12:39:09",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    c <- ggplot() +
        geom_line(data = df_plot, aes(x = ds, y = temperature_values, color = temperature_type), size = 0.5) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air Temperature ( ", degree*C, " )")), color = "Type of Temperature") +
        #scale_color_brewer(palette="Dark2") +
        xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(         
          axis.title=element_text(size=10),
          axis.text=element_text(size=10),
          legend.position="bottom",
          legend.box = "horizontal",
          legend.title=element_text(size=12), 
          legend.text=element_text(size=12)
        )+
        guides(color = guide_legend(ncol = 1, nrow = 3, byrow = TRUE))
    plot <- a / b / c
    plot + ggsave('../results/forecasting_analysis/forecast_temperatures_by_intervals_lines.png')
    plot + ggsave('../results/forecasting_analysis/forecast_temperatures_by_intervals_lines.pdf')
}

plot_forecasts_all_components_first_interval <- function() {
    df_plot <- df_result_forecasting_filtered
    start <- as.POSIXct("2019-11-20 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    end <- as.POSIXct("2020-01-03 10:31:28",format='%Y-%m-%d %H:%M:%S', tz = "GMT")

    c <- ggplot() +
        geom_line(data = df_plot, aes(x = ds, y = power, colour = "light green"), size = 0.5) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = "Power (W)") +
        #scale_color_brewer(palette="Dark2") +
        xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(         
          axis.text=element_text(size=10),
          axis.title.x=element_blank(), 
          legend.position="none",
          #legend.position = c(0.60, 0.95),
          #legend.direction = "horizontal",
          #legend.background = element_rect(fill = "white", color = "gray"),
          legend.title=element_text(size=10), 
          legend.text=element_text(size=9)
        )

    b <- ggplot() +
        geom_line(data = df_plot, aes(x = ds, y = heatsink, colour = "green"), size = 0.5) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("      Heatsink \n Temperature ( ", degree*C, " )"))) +
        #scale_color_brewer(palette="Dark2") +
        xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(
          axis.text=element_text(size=10),
          axis.title.x=element_blank(),
          legend.position="none",
          legend.title=element_text(size=10), 
          legend.text=element_text(size=9)
        )

    a <- ggplot() +
        geom_line(data = df_plot, aes(x = ds, y = temperature_values, color = temperature_type), size = 0.5) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air Temperature ( ", degree*C, " )")), color = "Type of Temperature") +
        #scale_color_brewer(palette="Dark2") +
        xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(
          axis.title=element_text(size=10),
          axis.text=element_text(size=10),
          legend.position="bottom",
          legend.box = "horizontal",
          legend.title=element_text(size=12), 
          legend.text=element_text(size=12)
        ) +
        guides(color = guide_legend(ncol = 1, nrow = 3, byrow = TRUE))
    plot <- c / b / a
    plot + ggsave('../results/forecasting_analysis/forecast_all_components_first_interval.png')
    plot + ggsave('../results/forecasting_analysis/forecast_all_components_first_interval.pdf')
}

plot_forecasts_all_components <- function() {
    df_plot <- df_result_forecasting_filtered
    c <- ggplot() +
        geom_line(data = df_plot, aes(x = ds, y = power, colour = "light green"), size = 0.3) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = "Power (W)") +
        #ylim(0,NA) +
        #scale_color_brewer(palette="Dark2") +
        #xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(         
          axis.title=element_text(size=10),
          axis.title.x=element_blank(),
          axis.ticks = element_blank(),
          axis.text.x = element_blank(),
          axis.text.y = element_text(size=10),        
          legend.position="none"
        )

    b <- ggplot() +
        geom_line(data = df_plot, aes(x = ds, y = heatsink, colour = "green"), size = 0.3) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Heatsink(",degree*C,")"))) +
        scale_color_brewer(palette = "Set2") +
        #ylim(0, NA) +
        theme(
          axis.title=element_text(size=10),
          axis.title.x=element_blank(),
          axis.ticks = element_blank(),
          axis.text.x = element_blank(),
          axis.text.y = element_text(size=10),
          legend.position="none"
        )

    a <- ggplot() +
        geom_line(data = df_plot, aes(x = ds, y = temperature_values, color = temperature_type), size = 0.3) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air(",degree*C,")")), color = "Type of Temperature") +
        scale_color_brewer(palette = "Set2") +
        #ylim(0, NA) +
        theme(
          axis.title=element_text(size=10),
          axis.text = element_text(size=10),
          legend.direction = "horizontal",
          legend.position="bottom",
          #legend.box = "vertical",
          legend.title=element_text(size=10), 
          legend.text=element_text(size=10),
          legend.title.align = 0.5
        ) +
        guides(colour=guide_legend(ncol=3,byrow=TRUE, title.position = "top"))
        #guides(color = guide_legend(ncol = 1, nrow = 2, byrow = TRUE))
    plot <- c / b / a
    
    pdf(file = "../results/forecasting_analysis/forecasted_all_components.pdf", width = 4, height = 4)#, paper = "a4")#, units = "cm", res = 300)
    print(plot)
    dev.off()

    png(file = "../results/forecasting_analysis/forecasted_all_components.png", width = 18, height = 10, units = "cm", res = 300)
    print(plot)
    dev.off()
}

plot_forecasts_all_period <- function() {
    df_plot <- df_result_forecasting_filtered
    ggplot() +
        geom_line(data = df_plot, aes(x = ds, y = temperature_values, color = temperature_type), size = 0.5) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air Temperature ( ", degree*C, " )")), color = "Type of Temperature") +
        #scale_color_brewer(palette="Dark2") +
        #xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(
          legend.position="bottom",
          legend.box = "horizontal",
          legend.title=element_text(size=10), 
          legend.text=element_text(size=9)
        ) +
        ggsave('../results/forecasting_analysis/forecasts_by_model_line.png') +
        ggsave('../results/forecasting_analysis/forecasts_by_model_line.png')
    
    ggplot() +
        geom_point(data = df_plot, aes(x = ds, y = temperature_values, color = temperature_type), size = 0.5) +
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air Temperature ( ", degree*C, " )")), color = "Type of Temperature") +
        #scale_color_brewer(palette="Dark2") +
        #xlim(start, end) +
        scale_color_brewer(palette = "Set2") +
        #scale_colour_manual(values = c("gray40", "darkred")) +
        theme(
          legend.position="bottom",
          legend.box = "horizontal",
          legend.title=element_text(size=10), 
          legend.text=element_text(size=9)
        ) +
        ggsave('../results/forecasting_analysis/forecasts_by_model_point.png') +
        ggsave('../results/forecasting_analysis/forecasts_by_model_point.png')
}

error_analysis_by_model <- function() {
   models <- c('model_1', 'model_2')

    a <- df_result_forecasting_filtered %>% 
            filter(temperature_type == "original") %>% 
            select(-temperature_type) %>%
            rename(original_temperature = temperature_values)

    for (model in models) {
        print('model_1')

        b <- df_result_forecasting_filtered %>% 
            filter(temperature_type == model) %>% 
            select(-temperature_type) %>%
            rename(forecast_temperature = temperature_values)
        df_forecast <- inner_join(a, b, by = c('ds'='ds'))

        mape(df_forecast$original_temperature, df_forecast$forecast_temperature)
        mae(df_forecast$original_temperature, df_forecast$forecast_temperature)
        rmse(df_forecast$original_temperature, df_forecast$forecast_temperature)
    } 
}

error_analysis_model_2 <- function() {
    print('historgram')
    a <- df_result_forecasting_apmh %>% select(ds, yhat) %>% arrange(ds)
    b <- df_result_original %>% select(ds, yhat) %>% arrange(ds)

    df_all <- inner_join(a, b, by = c('ds'='ds'))
    names(df_all)[2] <- 'forecast_temperature'
    names(df_all)[3] <- 'original_temperature'
    
    df_all_error <- df_all %>% 
        select(ds, original_temperature, forecast_temperature) %>% 
        mutate(abs_error = abs(original_temperature - forecast_temperature))
    
    str(df_all_error$abs_error)
    str(df_all_error$abs_error[df_all_error$abs_error <= 0.5])
    str(df_all_error$abs_error[df_all_error$abs_error > 0.5])
    
    ggplot() +
        geom_point(data = df_all_error, aes(x = ds, y = abs_error), colour = "gray40", size = 0.5) + 
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("|Original Temperature - Forecast Temperature| ( ", degree*C, " )"))) +
        theme(axis.text=element_text(size=12)) +
    ggsave('../results/forecasting_analysis/abs_error_plot.png') +
    ggsave('../results/forecasting_analysis/abs_error_plot.pdf')
    
    ggplot(data = df_all_error, aes(x = abs_error)) +
        geom_histogram(fill = "gray60", bins = 20)+
        geom_vline(xintercept = 0.5, linetype="dotted", 
                    color = "red", size=1.0) +
        annotate("text", x = 1.5, y = 60000, label = "93.53% of the distribution", size = 6) +
        annotate("text", x = 1.5, y = 30000, label = "6.47% of the distribution", size = 6) +
        annotate("text", x = 2.9, y = 160000, label = "total number of samples = 301581", size = 6) +
        annotate(
            geom = "curve", x = 0.4, y = 60000, xend = 0.8, yend = 60000, 
            curvature = -.1, arrow = arrow(length = unit(2, "mm"))
        ) +
        annotate(
            geom = "curve", x = 0.6, y = 30000, xend = 0.8, yend = 30000, 
            curvature = -.1, arrow = arrow(length = unit(2, "mm"))
        ) +
        theme_bw()+ 
        labs(x = expression(paste("Temperature Forecasting Error ( ", degree*C, " )")), y = "Number of Samples") +
        scale_y_log10() +
        theme(
            axis.text=element_text(size=20),
            axis.title=element_text(size=20)) +
    ggsave('../results/forecasting_analysis/error_histogram.png', width = 24, height = 16, units = "cm") + 
    ggsave('../results/forecasting_analysis/error_histogram.pdf', width = 24, height = 16, units = "cm")
    print('historgram')
}

illustrate_methodology_plots <- function() {
    # To get the data from the original logs
    a <- df_result_original %>% select(-temperature_type, -power, -heatsink)
    names(a)[1] <- "original_temperature"
    
    # To get the data from model_2
    df_result_forecasting_apmh_filtered <- df_result_forecasting_apmh %>% 
                                    select(ds, yhat, yhat_lower, yhat_upper, temperature_type) %>% 
                                    distinct(ds,temperature_type, .keep_all = TRUE)
    
    b <- df_result_forecasting_apmh_filtered %>% select(-temperature_type, -yhat_lower, -yhat_upper)
    names(b)[2] <- "forecast_temperature"

    # To full join both data frames
    df_all <- full_join(a,b,by="ds")

    # To gather all temperature values in one column
    df_all_gathered <- df_all %>%
                            gather(
                                factor_key = TRUE, 
                                key = "temperature_type", 
                                value = "temperature_values",
                                -ds)

    # To filter the data to be plot
    df_plot <- df_all_gathered %>% select(ds, temperature_type, temperature_values) %>% 
                                    filter(temperature_type == "original_temperature" |
                                           temperature_type == "forecast_temperature") %>%
                                    distinct(ds, temperature_type, .keep_all = TRUE) %>%
                                    drop_na()
    
    # To plot
    
    # To define the min and max timestamp on the plot
    ds_min <- df_plot[17280:17280, 1]
    ds_max <- df_plot[457150:457150, 1]

    first_step_data <- df_plot[17280:24197, ]
    first_step_forecasting <- df_plot[456255:456460, ]

    second_step_data <- df_plot[17559:24480, ]
    second_step_forecasting <- df_plot[456460:456687, ]

    third_step_data <- df_plot[17850:24775, ]
    third_step_forecasting <- df_plot[456687:456908, ]

    fourth_step_data <- df_plot[18144:25060, ]
    fourth_step_forecasting <- df_plot[456908:457150, ]

    all_period_data <- df_plot[17280:25060, ]
    all_period_forecasting <- df_plot[456255:457150, ]
    
    # To define the breakpoints of the x-ticks
    breakpoints <- c("2019-11-23 13:00:00", "2019-11-24 09:00:00")
    breakpoints <- as.POSIXct(breakpoints, format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    breakpoints_final <- c("2019-11-23 13:00:00", "2019-11-23 23:00:00", "2019-11-24 09:00:00")
    breakpoints_final <- as.POSIXct(breakpoints_final, format='%Y-%m-%d %H:%M:%S', tz = "GMT")

    # To define the position to print the labels (step 1, 2, 3, 4, combination)
    annotate_position <- as.POSIXct("2019-11-24 10:00:00",format='%Y-%m-%d %H:%M:%S', tz = "GMT")
    annotate_position_final <- as.POSIXct("2019-11-24 10:00:00",format='%Y-%m-%d %H:%M:%S', tz = "GMT")

    p1 <- ggplot() +
        geom_line(data = first_step_data, aes(x = ds, y = temperature_values), colour = "gray60", size = 1) + 
        geom_line(data = first_step_forecasting, aes(x = ds, y = temperature_values), colour = "darkred", size = 1) + 
        annotate("text", x = annotate_position, y = 11, label = "step 1", size = 4, fontface = "bold") + 
        scale_x_datetime(breaks = breakpoints, limit = c(ds_min, ds_max), date_labels = "%b %d %H:%M")+
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air ( ", degree*C, " )"))) +
        #scale_color_brewer(palette="Dark2") +
        #xlim(ds_min, ds_max) + 
        ylim(10, 25) + 
        theme(
            axis.title.x = element_blank(),
            axis.text.x = element_blank(),
            axis.ticks.x = element_blank(),
            legend.position="none",
            axis.title=element_text(size=11),
            axis.text=element_text(size=11))

    p2 <- ggplot() +
        geom_line(data = second_step_data, aes(x = ds, y = temperature_values), colour = "gray60", size = 1) + 
        geom_line(data = second_step_forecasting, aes(x = ds, y = temperature_values), colour = "darkred", size = 1) + 
        annotate("text", x = annotate_position, y = 11, label = "step 2", size = 4, fontface = "bold") + 
        scale_x_datetime(breaks = breakpoints, limit = c(ds_min, ds_max), date_labels = "%b %d %H:%M")+
        theme_bw()+
        labs(x = "Time (Date/Time)")+ #, y = " CPU Temperature (ºC)") +
        #scale_color_brewer(palette="Dark2") +
        #xlim(ds_min, ds_max) + 
        ylim(10, 25) +     
        theme(
            axis.title=element_blank(),
            axis.text=element_blank(),
            axis.ticks.y = element_blank(),
            axis.ticks.x = element_blank(),
            legend.position="none")

    p3 <- ggplot() +
        geom_line(data = third_step_data, aes(x = ds, y = temperature_values), colour = "gray60", size = 1) + 
        geom_line(data = third_step_forecasting, aes(x = ds, y = temperature_values), colour = "darkred", size = 1) + 
        annotate("text", x = annotate_position, y = 11, label = "step 3", size = 4, fontface = "bold") + 
        scale_x_datetime(breaks = breakpoints, limits = c(ds_min, ds_max), date_labels = "%b %d %H:%M")+
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air ( ", degree*C, " )"))) +
        #scale_color_brewer(palette="Dark2") +
        #xlim(ds_min, ds_max) + 
        ylim(10, 25) + 
        theme(legend.position="none",
            axis.title=element_text(size=11),
            axis.text=element_text(size=11))

    p4 <- ggplot() +
        geom_line(data = fourth_step_data, aes(x = ds, y = temperature_values), colour = "gray60", size = 1) +
        geom_line(data = fourth_step_forecasting, aes(x = ds, y = temperature_values), colour = "darkred", size = 1) + 
        annotate("text", x = annotate_position, y = 11, label = "step 4", size = 4, fontface = "bold") + 
        scale_x_datetime(breaks = breakpoints, limits = c(ds_min, ds_max), date_labels = "%b %d %H:%M")+ 
        theme_bw()+
        labs(x = "Time (Date/Time)")+#, y = " CPU Temperature (ºC)") +
        #scale_color_brewer(palette="Dark2") +
        #xlim(ds_min, ds_max) + 
        ylim(10, 25) + 
        theme(axis.title.y=element_blank(),
            legend.position="none",
            axis.text.y=element_blank(),
            axis.ticks.y = element_blank(),
            axis.title=element_text(size=11),
            axis.text=element_text(size=11))

    p5 <- ggplot() +
        geom_line(data = all_period_data, aes(x = ds, y = temperature_values, color = temperature_type), size = 1) +     
        geom_line(data = all_period_forecasting, aes(x = ds, y = temperature_values, color = temperature_type), size = 1) +     
        annotate("text", x = annotate_position_final, y = 11, label = "steps combination", size = 4, fontface = "bold") + 
        scale_x_datetime(breaks = breakpoints_final, limits = c(ds_min, ds_max), date_labels = "%b %d %H:%M")+
        #scale_x_datetime(date_labels = "%b %d %H:%M")+
        theme_bw()+
        labs(x = "Time (Date/Time)", y = expression(paste("Air ( ", degree*C, " )")), color = "Type of Temperature") +
        #scale_color_brewer(palette="Dark2") +
        scale_colour_manual(values = c("darkred", "gray70")) +
        #xlim(ds_min, ds_max) + 
        ylim(10, 25) + 
        theme(legend.position = "bottom",
            legend.title=element_text(size=14), 
            legend.text=element_text(size=14),
            axis.text.x=element_text(size=12),
            axis.text.y=element_text(size=12))

    plot <- (p1 | p2 )/ (p3 | p4) / p5
    plot + ggsave('../results/forecasting_analysis/forecasting_methodology.png')
    plot + ggsave('../results/forecasting_analysis/forecasting_methodology.pdf')
}

# Analysis Execution

# Set the timezone as UTF-8, for the plots
Sys.setlocale("LC_ALL", "en_GB.UTF-8")

# Read the files
df_result_original <- read.csv(file = '../input_data/filtered_input_data.csv', sep = ',', stringsAsFactors = FALSE)
df_result_original$time <- as.POSIXct(df_result_original$time,format='%Y-%m-%d %H:%M:%S', tz = "GMT")
df_result_original <- df_result_original %>%
            select(ambient_temperature, time, power_instant_main, heatsink_temperature) %>%
            rename(yhat = ambient_temperature, ds = time, power = power_instant_main, heatsink = heatsink_temperature) %>%
            arrange(ds) %>% 
            drop_na()
df_result_original$temperature_type <- 'original data'
#str(df_result_original)

df_result_forecasting_apm <- read.csv(file = '../results/forecasting/df_predictions_model_1.csv', 
                   sep = ',', stringsAsFactors = FALSE)
df_result_forecasting_apm$ds <- as.POSIXct(df_result_forecasting_apm$ds,format='%Y-%m-%d %H:%M:%S', tz = "GMT")
df_result_forecasting_apm <- df_result_forecasting_apm %>% 
            arrange(ds) %>% 
            drop_na()
df_result_forecasting_apm$temperature_type <- 'model 1'
#str(df_result_forecasting_apm)

df_result_forecasting_apmh <- read.csv(file = '../results/forecasting/df_predictions_model_2.csv', 
                   sep = ',', stringsAsFactors = FALSE)
df_result_forecasting_apmh$ds <- as.POSIXct(df_result_forecasting_apmh$ds,format='%Y-%m-%d %H:%M:%S', tz = "GMT")
df_result_forecasting_apmh <- df_result_forecasting_apmh %>% 
            arrange(ds) %>% 
            drop_na()
df_result_forecasting_apmh$temperature_type <- 'model 2'
#str(df_result_forecasting_apmh)

# Select the variables and join the data frames

a <- df_result_original %>% select(ds, yhat, heatsink, power) %>% rename(original = yhat)
b <- df_result_forecasting_apm %>% select(ds, yhat) %>% rename(model_1 = yhat)
c <- df_result_forecasting_apmh %>% select(ds, yhat) %>% rename(model_2 = yhat)

df_result_forecasting <- inner_join(a,b, by = c('ds'='ds'))
df_result_forecasting <- inner_join(df_result_forecasting,c, by = c('ds'='ds'))

df_results_gathered <- df_result_forecasting %>% 
                        gather(
                            factor_key = TRUE, 
                            key = "temperature_type", 
                            value = "temperature_values",
                            -ds, -heatsink, -power) %>%
                        distinct(ds,temperature_type, .keep_all = TRUE)
#str(df_results_gathered)

df_result_forecasting_filtered <- df_results_gathered

# Execute the plots

#plot_forecasts_by_intervals_with_point()
#plot_forecasts_by_intervals_with_lines()
#plot_forecasts_all_components_first_interval()
#plot_forecasts_all_components()
#plot_forecasts_all_period()
#error_analysis_by_model()
#error_analysis_model_2()
illustrate_methodology_plots()