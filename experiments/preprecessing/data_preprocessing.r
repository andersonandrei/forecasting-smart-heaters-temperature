# Importing libraries
library(dplyr)
library(tidyr)

# To read the input file
df_rad <- read.csv(file = '../input_data/input_data.csv', sep = ',', stringsAsFactors = FALSE)

# To filter all variables
df_rad_filtered <- df_rad %>% filter(
                                ambient_temperature >= 0 & 
                                ambient_temperature <= 85 &
                                heatsink_temperature >= 0 &
                                heatsink_temperature <= 85 &
                                cpu_temperature >= 0 &
                                cpu_temperature <= 85 &
                                motherboard_cpu_temperature >= 0 &
                                motherboard_cpu_temperature <= 85 &
                                target_temperature >= 0 &
                                target_temperature <= 85 &
                                power_instant_main >= 0 &
                                power_instant_main <= 1000 &
                                power_instant_aux >= 0 &
                                power_instant_aux <= 1000 &
                                cpu_instant_power >= 0 &
                                cpu_instant_power <= 1000) %>%
                            select(-X)

# To as cvs
write.csv(df_rad_filtered, '../input_data/filtered_input_data.csv')